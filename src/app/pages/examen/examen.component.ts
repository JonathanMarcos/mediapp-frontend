import { Component, OnInit, ViewChild } from '@angular/core';
import { ExamenService } from '../../_service/examen.service';
import { Examen } from '../../_model/examen';
import { MatTableDataSource, MatSnackBar, MatSort, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-examen',
  templateUrl: './examen.component.html',
  styleUrls: ['./examen.component.css']
})
export class ExamenComponent implements OnInit {

  lista: Examen[] = [];
  datasource: MatTableDataSource<Examen>;
  displayedColumns = ['idExamen', 'nombre', 'descripcion', 'acciones'];
  @ViewChild(MatSort) sort: MatSort;
  paginator: MatPaginator;

  constructor(private examenService: ExamenService, private snackBar: MatSnackBar) { }

  ngOnInit() {

    this.examenService.examenCambio.subscribe(data => {
      this.lista = data;
      this.datasource = new MatTableDataSource(this.lista);
      this.datasource.sort = this.sort;
      this.datasource.paginator = this.paginator;

      this.examenService.mensaje.subscribe(data => {
        this.snackBar.open(data, 'Aviso', { duration: 2000 });
      });
    });

    this.examenService.listarExamenes().subscribe(data => {
      this.lista = data;
      this.datasource = new MatTableDataSource(this.lista);
      this.datasource.sort = this.sort;
      this.datasource.paginator = this.paginator;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLocaleLowerCase();
    this.datasource.filter = filterValue;
  }

  eliminar(id: number) {
    this.examenService.eliminar(id).subscribe(data => {
      this.examenService.listarExamenes().subscribe(listado => {
        this.lista = listado;
        this.datasource = new MatTableDataSource(this.lista);
        this.datasource.sort = this.sort;
        this.datasource.paginator = this.paginator;
      });
    });
  }

}
