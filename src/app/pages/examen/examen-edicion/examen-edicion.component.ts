import { Component, OnInit } from '@angular/core';
import { ExamenService } from '../../../_service/examen.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Examen } from '../../../_model/examen';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-examen-edicion',
  templateUrl: './examen-edicion.component.html',
  styleUrls: ['./examen-edicion.component.css']
})
export class ExamenEdicionComponent implements OnInit {

  id: number;
  examen: Examen;
  form: FormGroup;
  edicion = false;

  constructor(private examenService: ExamenService, private route: ActivatedRoute, private router: Router) {
    this.examen = new Examen();
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'descripcion': new FormControl('', [Validators.required, Validators.minLength(5)])
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  initForm(){
    if (this.edicion){
      this.examenService.listarExamenPorId(this.id).subscribe( (data: Examen) => {
        let id = data.idExamen;
        let nombre = data.nombre;
        let descripcion = data.descripcion;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'nombre': new FormControl(nombre),
          'descripcion': new FormControl(descripcion)
        });

      });
    }
  }

  operar(){
    this.examen.idExamen = this.form.value['id'];
    this.examen.nombre = this.form.value['nombre'];
    this.examen.descripcion = this.form.value['descripcion'];

    if (this.edicion){
      this.examenService.modificar(this.examen).subscribe(data => {
        this.examenService.listarExamenes().subscribe(examenes => {
          this.examenService.examenCambio.next(examenes);
          this.examenService.mensaje.next('Se registró');
        });
      });
    }else {
      this.examenService.registrar(this.examen).subscribe(data => {
        this.examenService.listarExamenes().subscribe(examenes => {
          this.examenService.examenCambio.next(examenes);
          this.examenService.mensaje.next('Se registró');
        });
      });
    }

    this.router.navigate(['examen']);
  }

}
