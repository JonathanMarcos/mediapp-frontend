import { Component, OnInit } from '@angular/core';
import { Especialidad } from '../../../_model/especialidad';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EspecialidadService } from '../../../_service/especialidad.service';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-especialidad-edicion',
  templateUrl: './especialidad-edicion.component.html',
  styleUrls: ['./especialidad-edicion.component.css']
})
export class EspecialidadEdicionComponent implements OnInit {

  id: number;
  especialidad: Especialidad;
  form: FormGroup;
  edicion = false;

  constructor(private especialidadService: EspecialidadService, private route: ActivatedRoute, private router: Router) {
    this.especialidad = new Especialidad();
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl('', [Validators.required, Validators.minLength(3)])
    });
   }

  ngOnInit() {
    this.route.params.subscribe((param: Params) => {
      this.id = param['id'];
      this.edicion = param['id'] != null;
      this.initForm();
    });
  }

  initForm(){
    if (this.edicion){
      this.especialidadService.listarEspecialidadPorId(this.id).subscribe(data => {
        let id = data.idEspecialidad;
        let nombre = data.nombre;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'nombre': new FormControl(nombre)
        });
      });
    }
  }

  operar() {
    this.especialidad.idEspecialidad = this.form.value['id'];
    this.especialidad.nombre = this.form.value['nombre'];

    if (this.edicion){
      // update
      this.especialidadService.modificar(this.especialidad).subscribe(data => {
        this.especialidadService.listarEspecialidades().subscribe(especialidades => {
          this.especialidadService.especialidadCambio.next(especialidades);
          this.especialidadService.mensaje.next('Se modificó especialidad');
        });
      });
    }else {
      // insert
      this.especialidadService.registrar(this.especialidad).subscribe(data => {
        this.especialidadService.listarEspecialidades().subscribe(especialidades => {
          this.especialidadService.especialidadCambio.next(especialidades);
          this.especialidadService.mensaje.next('Se registro especialidad');
        });
      });
    }
    this.router.navigate(['especialidad']);
  }

}
