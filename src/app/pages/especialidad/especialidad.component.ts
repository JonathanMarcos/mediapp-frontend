import { Component, OnInit, ViewChild } from '@angular/core';
import { EspecialidadService } from '../../_service/especialidad.service';
import { Especialidad } from '../../_model/especialidad';
import { MatTableDataSource, MatSnackBar, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-especialidad',
  templateUrl: './especialidad.component.html',
  styleUrls: ['./especialidad.component.css']
})
export class EspecialidadComponent implements OnInit {

  lista: Especialidad[] = [];
  displayedColumns = ['idEspecialidad', 'nombre', 'acciones'];
  datasource: MatTableDataSource<Especialidad>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private especialidadService: EspecialidadService, private snackBar: MatSnackBar) { }

  ngOnInit() {

    this.especialidadService.especialidadCambio.subscribe(data => {
      this.lista = data;
      this.datasource = new MatTableDataSource(this.lista);
      this.datasource.paginator = this.paginator;
      this.datasource.sort = this.sort;

      this.especialidadService.mensaje.subscribe(data => {
        this.snackBar.open(data, 'Aviso', { duration: 2000 });
      });
    });

    this.especialidadService.listarEspecialidades().subscribe(data => {
      this.lista = data;
      this.datasource = new MatTableDataSource(this.lista);
      this.datasource.paginator = this.paginator;
      this.datasource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.datasource.filter = filterValue;
  }

  eliminar(id: number){
    this.especialidadService.eliminar(id).subscribe(data => {
      this.especialidadService.listarEspecialidades().subscribe(dataa => {
        this.lista = dataa;
        this.datasource = new MatTableDataSource(this.lista);
        this.datasource.paginator = this.paginator;
        this.datasource.sort = this.sort;
      });
    });
  }

}
